package main

import "testing"

// Test body func
func utBody(testSetSize int, testSet [16][3]string, e *testing.T) {
	for i := 0; i < testSetSize; i++ {
		out := pokerLogic(testSet[i])
		if testSet[0][0] != out {
			e.Errorf("|Poker Logic|-UT #%v ('%v') failed. For input [%v] [%v] obtained: '%v'",
				i, testSet[0][0], testSet[i][1], testSet[i][2], out) // to indicate test failed
		}
	}
}

// Test "First hand wins!" set
func TestPokerLogic1HWins(t *testing.T) {
	const testSetLen int = 16 // TODO: Extract arr dimension autom-ly

	var tstSetData = [...][3]string{
		{"First hand wins!",
			"AAAQQ", "QQQAA"},
		{"", "Q53Q4", "53QQ2"},
		{"", "53888", "88375"},
		{"", "33337", "QQAAA"},
		{"", "22333", "AAA58"},
		{"", "33389", "AAKK4"},
		{"", "44223", "AA892"},
		{"", "22456", "AKQJT"},
		{"", "99977", "77799"},
		{"", "99922", "88866"},
		{"", "9922A", "9922K"},
		{"", "99975", "99965"},
		// {"", "99965", "99975"}, // DB: Second Win -failure
		{"", "99975", "99974"},
		{"", "99752", "99652"},
		{"", "99752", "99742"},
		{"", "99753", "99752"},
	}

	utBody(testSetLen, tstSetData, t)
}

// Test "Second hand wins!" set
func TestPokerLogic2HWins(t *testing.T) {
	const testSetLen int = 16 // TODO: Extract arr dimension autom-ly

	var tstSetData = [...][3]string{
		{"Second hand wins!",
			"QQQAA", "AAAQQ"},
		{"", "53QQ2", "Q53Q4"},
		{"", "88375", "53888"},
		{"", "QQAAA", "33337"},
		{"", "AAA58", "22333"},
		{"", "AAKK4", "33389"},
		{"", "AA892", "44223"},
		{"", "AKQJT", "22456"},
		{"", "77799", "99977"},
		{"", "88866", "99922"},
		{"", "9922K", "9922A"},
		{"", "99965", "99975"},
		{"", "99974", "99975"},
		{"", "99652", "99752"},
		// {"", "Q53Q2", "53QQ2"}, // DB: Tie -failure
		{"", "99742", "99752"},
		{"", "99752", "99753"},
	}

	utBody(testSetLen, tstSetData, t)
}

// Test "It's a tie!" set
func TestPokerLogicTieWins(t *testing.T) {
	const testSetLen int = 6

	// FIXME: Again cannot convert variabe into fixed length array type
	var tstSetData = [16][3]string{
		{"It's a tie!",
			"AAAQQ", "QQAAA"},
		{"", "53QQ2", "Q53Q2"},
		{"", "53888", "88385"},
		{"", "QQAAA", "AAAQQ"},
		{"", "Q53Q2", "53QQ2"},
		// {"", "53888", "88375"}, // DB: First Win -failure
		{"", "88385", "53888"},
	}

	utBody(testSetLen, tstSetData, t)
}
