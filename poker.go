package main

import (
	"fmt"
	"os"
)

const cardSetCnt = 5 // card set count to compare

// Poker winner selector logic func
func pokerLogic(hand [3]string) string {

	var hist [2][15]int
	var capital [2]int

	cardsValMap := map[string]int{
		"2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7, "8": 8, "9": 9,
		"T": 10, "J": 11, "Q": 12, "K": 13, "A": 14,
	}

	// Calculate fuzzy cost function
	for n := 0; n <= 1; n++ {
		for i := 0; i < cardSetCnt; i++ {
			chStr := string(hand[n+1][i])
			pos := cardsValMap[chStr]
			hist[n][pos]++
		}
		dblPairs := 0
		for c := 2; c <= 14; c++ {
			pwi := 1
			for g := 2; g <= hist[n][c]; g++ {
				pwi *= 40 * pwi // exp-fact steep up
				dblPairs++
			}
			capital[n] += (pwi * c) * hist[n][c]
		}
		capital[n] += dblPairs * 2e7
	}

	// fmt.Println("DB -->  1st set Cap:", capital[0], ", 2nd set Cap:", capital[1]) // DB

	if capital[0] > capital[1] {
		return ("First hand wins!")
	} else if capital[0] < capital[1] {
		return ("Second hand wins!")
	}
	return ("It's a tie!")
}

// Main with input checks, poker logic application and output forming
func main() {
	var cliArgs [3]string

	argCnt := len(os.Args) - 1

	if argCnt != 2 {
		fmt.Println("Number of arguments ('card sets') given (", argCnt, ") is not equal to expected 2. Aborted.")
		os.Exit(1)
	}
	// FIXME: "cannot convert (type []string) to type [3]string", so manual copy (luckily just 2 elem-s)
	cliArgs[1] = os.Args[1]
	cliArgs[2] = os.Args[2]

	for j := 1; j <= 2; j++ {
		if len(cliArgs[j]) != cardSetCnt {
			fmt.Println("Number of characters ('card count in a set') in argument", j,
				"given (", cliArgs[j], ") is not equal to expected", cardSetCnt, ". Aborted.")
			os.Exit(1)
		}
	}

	// fmt.Println("DB -->  args Cnt:", argCnt, ", 1st arg:", cliArgs[1], ", 2nd arg:", cliArgs[2]) // DB

	fmt.Println(pokerLogic([3]string(cliArgs)))
}
