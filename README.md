# Go programming assignment

The goal of this assignment is to test your programming skills using Go language. We hope to
receive an answer that is written in idiomatic Go and is easy to read, understand and modify.
Use those features of the language that make sense to you in this case.
You can share the code with us by by zipping up the code and attaching it to an email or by
sharing it via a private GitHub repository (in that case, invite github user tarvaina as a
collaborator).

### ***Specs***
Given two poker hands determine which one wins. We use a simplified version of poker, where
the cards don't have suits and there are no flushes, straights or straight flushes. The two hands
are given as strings of five characters, where each character is one of 23456789TJQKA. The
answer should be "First hand wins!", "Second hand wins!" or "It's a tie!". A hand wins if it has a
more valuable combination than the other or if they have the same combination but the cards of
one are higher than the cards of the other.

*The combinations are (in order of value):*

 - four of a kind (e.g. 77377)  
 - full house (e.g. KK2K2) 
 - triples (e.g. 32666) 
 - two pairs (e.g. 77332) 
 - pair (e.g. 43K9K) 
 - high card (i.e. anything else, e.g. 297QJ) 


When the hands have the same combination, the winner is the one where the combination is
formed with higher cards. In the case of two pairs, the higher pair is compared first (e.g. 99662
wins 88776). In the case of full house, the triples are compared first and then the pair (e.g.
88822 wins QQ777). If the combinations are formed with the same cards, then the rest of the
cards are compared from the highest card to the lowest. E.g. when the hands are 7T2T6 and
TT753, the first one wins because TT=TT, 7=7 and 6>5.


---


## Examples

### Each of these should print:  ***It's a tie!***
```
go run poker.go AAAQQ QQAAA
go run poker.go 53QQ2 Q53Q2
go run poker.go 53888 88385
go run poker.go QQAAA AAAQQ
go run poker.go Q53Q2 53QQ2
go run poker.go 88385 53888
```

### Each of these should print:  ***First hand wins!***
```
go run poker.go AAAQQ QQQAA
go run poker.go Q53Q4 53QQ2
go run poker.go 53888 88375
go run poker.go 33337 QQAAA
go run poker.go 22333 AAA58
go run poker.go 33389 AAKK4
go run poker.go 44223 AA892
go run poker.go 22456 AKQJT
go run poker.go 99977 77799
go run poker.go 99922 88866
go run poker.go 9922A 9922K
go run poker.go 99975 99965
go run poker.go 99975 99974
go run poker.go 99752 99652
go run poker.go 99752 99742
go run poker.go 99753 99752
```

### Each of these should print:  ***Second hand wins!***
```
go run poker.go QQQAA AAAQQ
go run poker.go 53QQ2 Q53Q4
go run poker.go 88375 53888
go run poker.go QQAAA 33337
go run poker.go AAA58 22333
go run poker.go AAKK4 33389
go run poker.go AA892 44223
go run poker.go AKQJT 22456
go run poker.go 77799 99977
go run poker.go 88866 99922
go run poker.go 9922K 9922A
go run poker.go 99965 99975
go run poker.go 99974 99975
go run poker.go 99652 99752
go run poker.go 99742 99752
go run poker.go 99752 99753
```

---

### Package content

 * poker.go
 * poker_test.go

The code works () according to the description given
The second source file comprises the **tests** based on the all input and output wanted.
It can be run with:
`go test [-v]`

The test sequence output should look (*in this sample - for Win OS*) as follows:  

```
	PS C:\Path\To\Run\Folder> go test -v
=== RUN   TestPokerLogic1HWins
--- PASS: TestPokerLogic1HWins (0.00s)
=== RUN   TestPokerLogic2HWins
--- PASS: TestPokerLogic2HWins (0.00s)
=== RUN   TestPokerLogicTieWins
--- PASS: TestPokerLogicTieWins (0.00s)
PASS
ok      _/C_/Path\To\Run\Folder 9.500s
```

---

(C) Taken from test GitLab repo [here](https://bitbucket.org/ovt2019/iceye_go/src/master/)